#!/usr/bin/env node
// RJM 20-Nov-19

const ioRedis = require('ioredis')
const util = require('util')

const run = async () => {
  redis = new ioRedis()
  await redis.client('setname', 'readr')
  const info = await redis.info()
  const version = info.match(/redis_version:([0-9]+)\.[0-9.]+/)[1] 

  if (version >= 5) {
    
    const showList = (list) => {
      let idr = null
      list.map( l => {
	let [id, [, item]] = l
	idr = id // save last id for next iteration
	let obj = JSON.parse(item)
	console.log(`${obj.meta.timestamp} ${obj.level}: ${obj.message}` )
      })
      return idr
    }

    let time = await redis.time()
    startID = (time[0] - 3600) * 1000  // start one hour ago
    
    let winston = await redis.xread(['block', 0, 'count', 10, 'streams', 'winston', startID])
    while(winston) {
      let nextID = null
      winston.map( item => {
	let [stream, list] = item
	nextID = showList(list)
      })
      winston = await redis.xread(['block', 0, 'streams', 'winston', nextID])
    }
  }
  else {
    redis.quit()
    throw "Redis server has to have version of at least 5.0"
  }
  redis.quit()
}

run()
  .then()
  .catch((e) => console.log(e) )
