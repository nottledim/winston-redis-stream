# winston-redis-stream

A capped Redis transport for [winston][0].

## About

This package is a clone of winston-redis by Charlie Robbins, modified
to use the new Streams feature of Redis included from version 5.  It
uses ioRedis[1] rather than node-redis as the underlying client library
because that supports the new stream commands. Otherwise the API is
very similar.

The basic log method works.

The query method returns entries from the log selected by time:
```
const options = {
  from: new Date() - (24 * 60 * 60 * 1000),  // last 24 hrs
  until: new Date(),
  limit: 20,                                 // but only most recent 20 entries
  order: 'asc',                              // order earliest to most recent
  fields: ['level', 'message', 'timestamp']  // fields required
};
```

The "level" and "message" fields are always present. "timestamp" is a
meta field and other meta fields which exist can also be selected.

```
logger.query(options, function (err, results) {
  if (err) {
    /* TODO: handle me */
    throw err;
  }
  results.redis.map(log => {
    console.log(`${log.timestamp} ${log.level}: ${log.message}` )
  })
});
```

The  stream method has not been tried or tested and may well not work.

An example script "logreader.js" can be used to tail the log stream.

## Requirements

- NodeJS 8.11.x
- Winston 3.x
- Redis server version > 5.0.0

## Usage
``` js
  const winston = require('winston');
  const redisTransport = require('winston-redis-stream');

  const logger = winston.createLogger({
    level: 'info',
    transports: [
      new redisTransport()
    ]
  });

  logger.log({
    level: "info",
    message: "redis is awesome",
    reason: "it's fast" // this will get stored as meta data
  });
```

This transport accepts the options accepted by the [ioredis][1] client:

* __host:__ (Default **localhost**) Remote host of the Redis server
* __port:__ (Default **6379**) Port the Redis server is running on.
* __auth:__ (Default **None**) Password set on the Redis server

In addition to these, the Redis transport also accepts the following options.

* __redis:__ Either the redis client or the options for the redis client
* __length:__ (Default **200**) Number of log messages to store.
* __container:__ (Default **winston**) Name of the Redis container you wish your logs to be in.
* __channel:__ (Default **None**) Name of the Redis channel to stream logs from.
* __meta:__ (Default **{}**) Custom fields to add to each log.
* __flatMeta__: (Default **false**) Store meta data at the top level of the object rather than within a child `meta` object.

*Metadata:* Logged as JSON literal in Redis

## Installation

### Installing npm (node package manager)

``` bash
  $ curl http://npmjs.org/install.sh | sh
```

### Installing winston-redis

``` bash
  $ npm install winston
  $ npm install winston-redis-stream
```

## Run Tests
Winston-redis tests are written in [mocha][2], using [Abstract Winston Transport][3] and designed to be run with npm.

```
  npm test
```

#### Author: [Dick Middleton](http://gitlab/nottledim)
     Originator: [Charlie Robbins](http://github.com/indexzero)
     
[0]: https://github.com/winstonjs/winston
[1]: https://github.com/luin/ioredis
[2]: https://mochajs.org
[3]: https://github.com/winstonjs/abstract-winston-transport
